"""
The flask application package.

blog http://jibreel.me/blog/1/

"""

from flask import Flask, render_template, request, session
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
db.Model = Base


import crudo.views
import crudo.forms
