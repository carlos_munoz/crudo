"""
forms and views for the flask application.
"""
from flask.ext.wtf import Form
from wtforms import StringField, IntegerField, DecimalField, SelectField
from wtforms.validators import DataRequired

class TiendaForm(Form):
    estados=[
        (1,'AGUASCALIENTES'),
         (2,'BAJA CALIFORNIA'),
         (3,'BAJA CALIFORNIA SUR'),
         (4,'CAMPECHE'),
         (7,'CHIAPAS'),
         (8,'CHIHUAHUA'),
         (5,'COAHUILA DE ZARAGOZA'),
         (6,'COLIMA'),
         (20,'DISTRITO FEDERAL'),
         (9,'DURANGO'),
         (10,'GUANAJUATO'),
         (11,'GUERRERO'),
         (12,'HIDALGO'),
         (13,'JALISCO'),
         (14,'MEXICO'),
         (15,'MICHOACAN DE OCAMPO'),
         (16,'MORELOS'),
         (17,'NAYARIT'),
         (18,'NUEVO LEON'),
         (19,'OAXACA'),
         (21,'PUEBLA'),
         (22,'QUERETARO DE ARTEAGA'),
         (23,'QUINTANA ROO'),
         (24,'SAN LUIS POTOSI'),
         (25,'SINALOA'),
         (26,'SONORA'),
         (27,'TABASCO'),
         (28,'TAMAULIPAS'),
         (29,'TLAXCALA'),
         (30,'VERACRUZ'),
         (31,'YUCATAN'),
         (32,'ZACATECAS')
        ]

    tienda_id= IntegerField('id', default=0)
    nombre = StringField('nombre', validators=[DataRequired()])
    direccion= StringField('direccion', validators=[DataRequired()])
    codigo_postal= IntegerField('cp', validators=[DataRequired()])
    estado= SelectField('estado',choices=estados, validators=[DataRequired()])
    zona= StringField('zona')
    latitud= DecimalField('latitud', validators=[DataRequired()])
    longitud= DecimalField('longitud', validators=[DataRequired()])