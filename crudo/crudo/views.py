"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template
from crudo import app
from crudo.forms import TiendaForm

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Tiendas',
        year=datetime.now().year,
    )

@app.route('/tiendas', methods=['GET','POST'])
def tiendas():
    """Agregado de tiendas"""
    form = TiendaForm()
    return render_template(
        'tiendas.html',
        title='tiendas',
        form=form)